from bs4 import BeautifulSoup, Tag, Comment
import requests
from pymongo import MongoClient
import json
import urllib
import datetime
import sys

def get_notices(html_soup):
    '''
    Função que coleta as notícias destaque do site
    '''
    #procura todas as divs que possuem a classe "collection-item"
    notice_container = html_soup.find_all('div', class_ = 'collection-item')

    data = {}
    data['notices'] = []

    #cria objeto com as informações de cada notícia
    for cont in range(len(notice_container)):
        data['notices'].append({
            'title': notice_container[cont].a.text,
            'date': notice_container[cont].p.text,
            'link': notice_container[cont].a.get('href'),
            'create_date': datetime.datetime.now()
        })

    #retorna um dicionário com as informações
    return(data['notices'])

def get_all_link(url):
    '''
    Função que coleta todos os links com url do site
    '''
    #procura todas as tags a
    link_container = html_soup.find_all('a')

    data = {}
    data['links'] = []

    #cria objeto com as informações de cada link    
    for cont in range(len(link_container)):
        if link_container[cont].has_attr('href'):
            data['links'].append({
                'url': link_container[cont].get('href'),
                'create_date': datetime.datetime.now()
            })

    #retorna um dicionário com as informações
    return(data['links'])

def get_portals(html_soup):
    '''
    Função que coleta todos os portais do site
    '''
    #procuta a primeira tag table
    portal_container = html_soup.table
    ths = []

    #recolhe todos as tagas th que estão nessa table, onde cada th é um portal
    for th in portal_container.find_all("th"):
        ths.append(th)
        
    data = {}
    data['portals'] = []

    #cria objeto com as informações de cada portal
    for cont in range(len(ths)):
        data['portals'].append({
            'title': ths[cont].img.get('title'),
            'link': ths[cont].a.get('href'),
            'create_date': datetime.datetime.now()
        })
    
    #retorna um dicionário com as informações
    return(data['portals'])

def get_menus(html_soup):
    '''
    Função que coleta todos os menus do site
    '''
    #procura a primeira div que possuí um id com valor 'sobre'
    menu_container = html_soup.find('div', attrs={'id':'sobre'})

    #seleciona todas as tags li dentro da div
    lis = []
    for li in  menu_container.find_all('li'):
        lis.append(li)
    
    data = {}
    data['menus'] = []
    
    #cria objeto com as informações de cada menu
    for cont in range(len(lis)):
        data['menus'].append({
            'title': lis[cont].a.text,
            'link': lis[cont].a.get('href'),
            'create_date': datetime.datetime.now()
        })

    #retorna um dicionário com as informações
    return(data['menus'])

def get_banners(html_soup):
    '''
    Função que coleta os banners do site, trabalhando com navegação entre pais
    '''
    data = {}
    data['banners'] = []

    #procura todas as tags span que possuem uma class com valor 'faixa' e cria o objeto
    for span in html_soup.find_all('span', attrs={'class':'faixa'}):
        data['banners'].append({
            'title': span.parent.parent.find('span', attrs={'class':'title'}).text,
            'link': span.parent.parent.a.get('href'),
            'image_text': span.parent.parent.img.get('alt'),
            'number' : span.parent.parent.a.text,
            'create_date': datetime.datetime.now()
        })

    #retorna um dicionário com as informações
    return(data['banners'])

def get_texts(html_soup):
    '''
    Função que coleta os banners do site, trabalhando com navegação entre pais
    '''
    #procura todos os textos
    texts = html_soup.find_all(text=True)
    #seleciona os textos que são visiveis na página
    visible_texts = filter(tag_visible, texts)  
    visible_texts = list(filter(None, visible_texts))  

    text_list = []
    #corre o array da lista de textos visíveis para retirar espaços em branco
    for cont in range(len(visible_texts)):
        text_list.append(visible_texts[cont].strip())
    
    #retira todas as posições da lista que não possuem valor
    new_list = list(filter(None, text_list))

    data = {}
    data['texts'] = []

    #cria objeto com as informações dos textos
    for cont in range(len(new_list)):
        data['texts'].append({
            'text': new_list[cont],
            'create_date': datetime.datetime.now()
        })
    
    #retorna um dicionário com as informações
    return(data['texts'])

def tag_visible(element):
    '''
    Função que filtra as tags que são visiveis na página
    '''
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
        return False
    if isinstance(element, Comment):
        return False
    return True

def get_request(url):
    '''
    Função que realiza requisição para verificar se o site está disponível
    '''
    try:
        return requests.get(url)
    except (requests.exceptions.HTTPError, requests.exceptions.RequestException,
	requests.exceptions.ConnectionError, requests.exceptions.Timeout) as e:
        print(str(e))
    except Exception as e:
	    raise

def connect_mongo(db_name, db_collection, db_container):
    '''
    Função que realiza conexão com o banco de dados - Mongodb e adiciona a coleção de dados no banco
    '''
    #conecta com o cliente do bd
    client = MongoClient("mongodb+srv://ligia:" + urllib.parse.quote("2410mnj1706h") + "@webscrap-sygla.mongodb.net/" + db_name)
    #seleciona o banco de dados a ser utilizado
    db = client[db_name]
    #seleciona a coleção que os dados serão salvos
    collection = client.get_database(db_collection)
    #insere os dados na coleção
    result = client.db.collection.insert_many(db_container)
    #mostra os ids dos objetos que foram criados no banco de dados
    print("Banco de Dados: "+str(db_name))
    print("Ids dos objetos inseridos na coleção "+ str(db_collection)+":" + str(result.inserted_ids))

if __name__ == '__main__':
    
    #URL para Web Scrapping
    url = "http://idg.receita.fazenda.gov.br/"
    page = get_request(url)

    #Se retornar valor da requisição feita a página, busca link
    if page:
        #requisição
        response = requests.get(url)
        #tranforma o retorno da requisição em texto e assim em um objeto BeautifulSoup
        html_soup = BeautifulSoup(response.text, 'html.parser')

        #execução do arquivo, opções
        if '-all' in sys.argv:

            notices = get_notices(html_soup)
            if notices:
                connect_mongo('web_scrapping', 'notices', notices)

            links = get_all_link(html_soup)
            if links:
                connect_mongo('web_scrapping', 'links', links)
            
            portals = get_portals(html_soup)
            if portals:
                connect_mongo('web_scrapping', 'portals', portals)
                
            menus = get_menus(html_soup)
            if menus:
                connect_mongo('web_scrapping', 'menus', menus)

        elif '-n' in sys.argv:
            notices = get_notices(html_soup)
            if notices:
                connect_mongo('web_scrapping', 'notices', notices)

        elif '-l' in sys.argv:
            links = get_all_link(html_soup)
            if links:
                connect_mongo('web_scrapping', 'links', links)

        elif '-p' in sys.argv:
            portals = get_portals(html_soup)
            if portals:
                connect_mongo('web_scrapping', 'portals', portals)
        
        elif '-m' in sys.argv:
            menus = get_menus(html_soup)
            if menus:
                connect_mongo('web_scrapping', 'menus', menus)
        
        elif '-b' in sys.argv:
            banners = get_banners(html_soup)
            if banners:
                connect_mongo('web_scrapping', 'banners', banners)

        elif '-t' in sys.argv:
            texts = get_texts(html_soup)
            if texts:
                connect_mongo('web_scrapping', 'texts', texts)

        else:
            print('Arguments')
            print('-n --scrap only notices')
            print('-l  --scrap only all links')
            print('-p  --scrap only all portals')
            print('-m  --scrap only menus')
            print('-b  --scrap only banners')
            print('-t  --scrap only all texts')
            print('-all --all scraps')
