# Brasil 317 - web scrapping
Script que realiza coleta de dados do site: http://idg.receita.fazenda.gov.br/

Projeto realizado utilizando Python 3.6.4

## Começando

Utilize git clone https://gitlab.com/ligiafs/brasil317.git para fazer o download do código

### Pré-requisitos

Python 3.6.4 - link para download: https://www.python.org/

### Instalação

Primeiro Passo

Instalar o driver de conexão Python/Mongodb - PyMongo3.4 e suas dependências

Execute: 
py -m easy_install pymongo

Após o término, execute: 
py -m pip install pymongo[gssapi,srv,tls]

Instalar a biblioteca BeatifulSoup

Execute:
py -m pip install beautifulsoup4

### Uso

py web_scrapping.py -all

